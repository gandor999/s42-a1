// Using DOM
// Retrieve an element from webpage

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector(`#txt-last-name`);
let spanFirstName = document.querySelector('#span-first-name');
let spanLastName = document.querySelector(`#span-last-name`)

// Alternative in retrieving an element - getElement

/*
	document.getElementById('txt-first-name');
	document.getElementsByClassName('txt-inputs');
	document.getElementsByTagName('input');
*/


// Activity

// Two Ways to do this


// First is with callback

let changeValue = function (e) {
	if(e.target === txtFirstName){
		spanFirstName.innerHTML = e.target.value;
	}

	if(e.target === txtLastName){
	 	spanLastName.innerHTML = e.target.value;
	}
}


txtFirstName.addEventListener('keyup', changeValue);
txtLastName.addEventListener('keyup', changeValue);


// Second is with calling the function normally


/*function changeValue (elementToChange, inputElement) {
	elementToChange.innerHTML = inputElement.value;
}

txtFirstName.addEventListener('keyup', (e) => {
	changeValue(spanFirstName, txtFirstName);
});

txtLastName.addEventListener('keyup', (e) => {
	changeValue(spanLastName, txtLastName);
});
*/